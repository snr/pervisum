# Notes sur les schémas

## Schéma - 22 juillet 2024

### Schéma modélisant la circulation des données "IIIF" dans l'application

L'idée principale de ce [schéma](https://gitlab.inha.fr/snr/pervisum/-/blob/934892af11a1aad70751488781e55118e2524ca0/DOCUMENTATION/schemas-workflow/captures-ecran/workflow_IIIF_projet_darticle.png) est que l'application fonctionne sans stocker une seule image au cours du process de création de l'article.

Il s'agit donc d'utiliser le protocole IIIF au maximum.

![](https://gitlab.inha.fr/snr/pervisum/-/raw/main/DOCUMENTATION/schemas-workflow/captures-ecran/workflow_IIIF_projet_darticle.png)

Pour le moment, on part du principe que  l'utilisateur dispose forcément d'un lien de manifeste IIIF (et non un lien URL Image par exemple). Ce lien s'ajoute donc dans une page/interface permettant de se constituer une liste de manifestes dans lesquels on souhaite piocher des images  pour son article.

Si l'ajout est confirmé par l'utilisateur, le lien du manifeste est enregistré dans un manifeste IIIF de type [Collection](https://iiif.io/api/presentation/3.0/#51-collection). Côté utilisateur, le titre du manifeste s'affiche dans sa liste de manifestes "sources" (car contenant la matière source pour l'iconographie de son article).

![](https://gitlab.inha.fr/snr/pervisum/-/raw/main/DOCUMENTATION/schemas-workflow/captures-ecran/3_workflow_IIIF_projet_darticle/Image1.png)

En sélectionnant un manifeste dans sa liste, l'utilisateur accède à une nouvelle interface affichant toutes les images contenues dans le manifeste. Il peut sélectionner celles qu'il souhaite utiliser pour son article et valide.

![](https://gitlab.inha.fr/snr/pervisum/-/raw/main/DOCUMENTATION/schemas-workflow/captures-ecran/3_workflow_IIIF_projet_darticle/Image2.png)

Les liens des images sont enregistrés en tant que canevas dans un manifeste "edition" (conforme à l'[Api présentation v3](https://iiif.io/api/presentation/3.0/)) qui sera finalement le fichier contenant tous les choix éditoriaux effectués sur les images (images sélectionnées, mise en ordre, annotations...). 

![](https://gitlab.inha.fr/snr/pervisum/-/raw/main/DOCUMENTATION/schemas-workflow/captures-ecran/3_workflow_IIIF_projet_darticle/Image3.png)

Toutes les images sélectionnées sont consultables sur une interface servant de "banque d'images". La banque d'images affiche tous les items/canevas du manifeste édition. Ce sont les images "originales" : tout le travail d'annotation, de recadrage/manipulation et de mise en ordre sera enregistré dans une autre partie du manifeste "edition": la partie "[structures](https://iiif.io/api/presentation/3.0/#structures)". Cette organisation permet de garder une distinction entre l'image "brute" et "travaillée".

![](https://gitlab.inha.fr/snr/pervisum/-/raw/main/DOCUMENTATION/schemas-workflow/captures-ecran/3_workflow_IIIF_projet_darticle/Image4.png)

L'utilisateur peut ensuite organiser son futur article en sous-corpus/thématiques selon ses besoins éditoriaux. Chaque choix de structuration se traduit en un niveau de "ranges" dans la partie "structures" du manifeste "édition". Dans ces "ranges" sont enregistrées les manipulations et annotations de l'image. Une image peut donc être présente plusieurs fois avec des affichages différents. Le travail d'annotation se fait dans une visionneuse IIIF et l'enregistrement du travail envoie les informations dans le manifeste "édition".

![](https://gitlab.inha.fr/snr/pervisum/-/raw/main/DOCUMENTATION/schemas-workflow/captures-ecran/3_workflow_IIIF_projet_darticle/Image5.png)

## Schémas au 16 juin 2024

### Synthèse des ateliers avec les éditeurs
Le [premier schéma](https://gitlab.inha.fr/snr/pervisum/-/blob/feb217acc118c9df55ccc57313067870e85ce111/DOCUMENTATION/schemas-workflow/captures-ecran/1_synthese_ateliers.png)  est une synthèse des discussions issues des ateliers organisés avec des éditeurs et des chercheurs. Il est réalisé à partir de la synthèse faite par Bulle et Juliette.
Il reprend sous forme de chaîne les différents besoins/envies qui ont emané des discussions.

![](https://gitlab.inha.fr/snr/pervisum/-/raw/feb217acc118c9df55ccc57313067870e85ce111/DOCUMENTATION/schemas-workflow/captures-ecran/1_synthese_ateliers.png)

### Début de workflow pour l'application
Le [deuxième schéma](https://gitlab.inha.fr/snr/pervisum/-/blob/main/DOCUMENTATION/schemas-workflow/captures-ecran/2_workflow_ajout_corpus.png) propose un workflow de récupération du corpus et de chargement dans l'application. 

![schema](https://gitlab.inha.fr/snr/pervisum/-/raw/main/DOCUMENTATION/schemas-workflow/captures-ecran/2_workflow_ajout_corpus.png)

Il propose une situation où le chercheur transmet l'ensemble de son article (texte + images + indications des annotations à mettre sur les images).

![sous schéma 1](https://gitlab.inha.fr/snr/pervisum/-/raw/main/DOCUMENTATION/schemas-workflow/captures-ecran/2_workflow_sous-schema/Image1.png)

*Note : le excel à remplir, ce n'est pas très pratique et agréable*

C'est donc l'éditeur qui a la main sur l'outil. Côté images, la première étape est de vérifier si les images sont hébergées en IIIF.

- Il existe un hébergement IIIF : l'éditeur récupère le lien du manifeste ou de l'URL de l'image.
- Il n'existe pas d'hébergement IIIF : les images peuvent être déposées sur Nakala qui propose une API Image. Il faudra penser une solution pour reconstituer les liens des images car les liens IIIF ne sont pas disponibles depuis l'interface de visualisation des données. L'utilisateur pourrait ajouter le lien dans l'application qui se chargerait de reconstituer le lien IIIF Image.

![sous schéma 2](https://gitlab.inha.fr/snr/pervisum/-/raw/main/DOCUMENTATION/schemas-workflow/captures-ecran/2_workflow_sous-schema/Image2.png)

**L'application doit pouvoir gérer l'ajout de manifestes en v2 et en v3.**

Dans le cas d'ajout de manifeste, il faut prévoir un affichage des images contenues dans le manifeste afin que l'utilisateur sélectionne la ou les images qu'il souhaite utiliser dans l'article. Il faudrait peut-être également prévoir un moyen de garder le lien entre l'image et le manifeste contenant l'entièreté de la source (à injecter dans une métadonnée ? *à discuter*).

Après avoir sélectionné les images à éditer, l'outil doit permettre de les ordonner et de les organiser en collection. C'est un besoin qui ressort fortement des ateliers avec les éditeurs.

Au sein de la collection, il sera possible d'ouvrir chaque image dans une visionneuse qui sera le cadre de l'annotation. Là encore, les annotations doivent pouvoir être ordonnées pour que l'utilisateur définisse le cheminement du lecteur sur l'image. 

Les annotations doivent également pouvoir être "typées" pour différencier les annotations contenant le coeur du propos scientifique de celles qui auraient plutôt un rôle de "note de bas de page". Cette mise en forme doit pouvoir être faite au sein d'un même bloc d'annotation qui par exemple contiendrait à la fois une réflexion sur une partie d'image sélectionnée, un tag et une référence bibliographique.

![sous schéma 3](https://gitlab.inha.fr/snr/pervisum/-/raw/main/DOCUMENTATION/schemas-workflow/captures-ecran/2_workflow_sous-schema/Image3.png)

*Il faudra sûrement couper le schéma en 2 :* 

*- un schéma sur la vérification de l'hébergement IIIF et le chargement de ces liens*

*- un schéma sur la gestion des images une fois importée dans l'application*

