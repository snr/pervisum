# Panel d'exemples de manifests IIIF

Plusieurs exemples de manifests IIIF sont présentés dans [ce dossier](https://gitlab.inha.fr/snr/pervisum/-/tree/main/DATA/experimentations_manifests/manifests_tests). Ils ont été pris parmi ceux mis à disposition sur [la page des recettes](https://iiif.io/api/cookbook/) du Consortium IIIF. Un a été généré par le plugin IIIF serveur d'Omeka-S.

1. Manifest simple, comprenant une image, sans annotation 
2. Manifest avec une image et une annotation
3. Manifest comprenant plusieurs images et des légendes (exporté grâce au plugin d'Omeka-S)
4. Manifest comprenant plusieurs images, recréé à partir de l'exemple fourni sur le livre de recettes du Consortium IIIF
5. Manifest comprenant des annotations sous la forme de tag
6. Manifest intégrant des annotations sur la forme de polygons rectangulaires
7. Manifest intégrant du HTML dans les annotations
8. Manifest intégrant des images dans les annotations

*La liste n'est pas exhaustive et peut être amenée à évoluer*

## Structure générale

Ce panel ne propose que des manifests de type *Manifest* - il ne comprend pas de manifest de type *Collection*.

Ces manifests respectent tous la structuration telle qu'elle a été définie selon la [V3 de l'API Presentation](https://iiif.io/api/presentation/3.0/)

```json
{
  "@context": "http://iiif.io/api/presentation/3/context.json",
  "id": "https://iiif.io/api/cookbook/recipe/0001-mvm-image/manifest.json",
  "type": "Manifest",
  "label": {
    "en": [
      "Single Image Example"
    ]
  },
  "items": [

      ...

  ]
}
```

* ```@context``` : URL renvoyant au fichier contenant les paramètres de configuration de la V3 de l'API Presentation IIIF, dans lequel s'inscrit le manifest
* ```id``` : URL du manifest
* ```type``` : le type de manifest (*Manifest* | *Collection*)
* ```label``` : Le titre du manifest, avec si nécessaire l'indication de la langue (ici *en*)
* ```items``` : première liste générique d'```items```, qui contiendra en l'occurence les ```items```de type *Canvas*

## Manifest simple, comprenant une image, sans annotation

Extrait du [premier manifest](https://gitlab.inha.fr/snr/pervisum/-/blob/main/DATA/experimentations_manifests/manifests_tests/1_simple_manifest_one_image.json) présent dans ```/manifests_tests```.

Cet extrait ne reprend que la partie de type *Canvas*.

```json 
{
      "id": "https://iiif.io/api/cookbook/recipe/0001-mvm-image/canvas/p1",
      "type": "Canvas",
      "height": 1800,
      "width": 1200,
      "items": [
        {
          "id": "https://iiif.io/api/cookbook/recipe/0001-mvm-image/page/p1/1",
          "type": "AnnotationPage",
          "items": [
            {
              "id": "https://iiif.io/api/cookbook/recipe/0001-mvm-image/annotation/p0001-image",
              "type": "Annotation",
              "motivation": "painting",
              "body": {
                "id": "http://iiif.io/api/presentation/2.1/example/fixtures/resources/page1-full.png",
                "type": "Image",
                "format": "image/png",
                "height": 1800,
                "width": 1200
              },
              "target": "https://iiif.io/api/cookbook/recipe/0001-mvm-image/canvas/p1"
            }
          ]
        }
      ]
    }
```

La V3 de l'API Presentation a adoptée une structuration plus génrique pour les manifests IIIF, via les ```items```.Chaque item a un ```type``` qui rend compte de la hiérarchie et de l'imbrication des éléments à l'intérieur du manifest.

L'objet de type *Canvas* est compris dans le tableau d'```items``` du manifest.
Cet objet comprend lui-même un tableau d'```items``` de type ```AnnotationPage```, qui comprend également un tableau d'```items``` de type ```Annotation```.

:warning: Attention, cette notion d'annotation peut être trompeuse et est bien utilisée, dans ce cas, pour faire référence à l'image IIIF.

* ```id``` : identifiant de l'image sous forme d'URL
* ```motivation``` : "*painting*" car il s'agit d'une image
* ```body``` : comprend les propriétés et valeur de la numérisation, avec un ```id``` qui indique l'URL pointant vers cette numérisation, le ```type```, le ```format```, la hauteur (```height```) et la largeur (```width```) de la numérisation.
* ```target``` : indique la relation entre l'image (*Annotation*) et le *Canvas*


## Manifest avec une image et une annotation

Extrait du [deuxième manifest](https://gitlab.inha.fr/snr/pervisum/-/blob/main/DATA/experimentations_manifests/manifests_tests/2_simple_manifest_one_image_one_annotation.json) présent dans ```/manifests_tests```



Les annotations en tant que telles sont comprises dans un tableau ```annotations``` à l'intérieur de l'```item``` de type *Canvas* auquel elles se réfèrent.


```json
"annotations": [
          {
            "id": "https://iiif.io/api/cookbook/recipe/0266-full-canvas-annotation/canvas-1/annopage-2",
            "type": "AnnotationPage",
            "items": [
              {
                "id": "https://iiif.io/api/cookbook/recipe/0266-full-canvas-annotation/canvas-1/annopage-2/anno-1",
                "type": "Annotation",
                "motivation": "commenting",
                "body": {
                  "type": "TextualBody",
                  "language": "de",
                  "format": "text/plain",
                  "value": "Göttinger Marktplatz mit Gänseliesel Brunnen"
                },
                "target": "https://iiif.io/api/cookbook/recipe/0266-full-canvas-annotation/canvas-1"
              }
            ]
          }
        ]
```

On retrouve à l'intérieur la même structuration en ```items``` de type *AnnotationPage* et *Annotation*, le ```body```, et les mêmes propriétés.
* ```motivation```: "*commenting*" et non plus "*painting*". Elle peut aussi devenir "*tagging*" dans le cas d'un *Canvas* ayant comme annotation un tag (une étiquette). Voir  le [cinquième manifest](https://gitlab.inha.fr/snr/pervisum/-/blob/main/DATA/experimentations_manifests/manifests_tests/5_manifest_tags.json)
* ```body```, de ```type``` *TextualBody*, un ```format``` de *text/plain* et en ```value``` le texte de l'annotation.

Les ```id``` sont toujours des URLs qui identifient l'item *AnnotationPage* et l'item *Annotation* et la ```target```renvoie toujours au *Canvas* auquel l'annotation se réfère

## Manifests avec plusieurs images 
### Manifest généré par Omeka-S

Voir le [troisième manifest](https://gitlab.inha.fr/snr/pervisum/-/blob/main/DATA/experimentations_manifests/manifests_tests/3_manifest_x_images_omeka-s.json) présent dans ```/manifests_tests```

### Manifest reconstitué d'après la V3 de l'API Presentation

Voir le [quatrième manifest](https://gitlab.inha.fr/snr/pervisum/-/blob/main/DATA/experimentations_manifests/manifests_tests/4_manifest_x_images.json) reconstitué manuellement, toujours d'après la V3 de l'API Presentation.
Dans ce cas, ce manifest comprend plusieurs ```items``` de type *Canvas*, chacun renvoyant à une image et à une numérisation.

## Manifest avec des annotations en polygons

Extrait du [sixième manifest](https://gitlab.inha.fr/snr/pervisum/-/blob/main/DATA/experimentations_manifests/manifests_tests/6_manifest_polygons.json) présent dans ```manifests_tests```


```json
"annotations": [
          {
            "id": "https://iiif.io/api/cookbook/recipe/0261-non-rectangular-commenting/page/p2/1",
            "type": "AnnotationPage",
            "items": [
              {
                "id": "https://iiif.io/api/cookbook/recipe/0261-non-rectangular-commenting/annotation/p0002-svg",
                "type": "Annotation",
                "motivation": "tagging",
                "body": {
                  "type": "TextualBody",
                  "value": "Gänseliesel-Brunnen",
                  "language": "de",
                  "format": "text/plain"
                },
                "target": {
                  "type": "SpecificResource",
                  "source": "https://iiif.io/api/cookbook/recipe/0261-non-rectangular-commenting/canvas/p1",
                  "selector": {
                    "type": "SvgSelector",
                    "value": "<svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink'><g><path d='M270.000000,1900.000000 L1530.000000,1900.000000 L1530.000000,1610.000000 L1315.000000,1300.000000 L1200.000000,986.000000 L904.000000,661.000000 L600.000000,986.000000 L500.000000,1300.000000 L270,1630 L270.000000,1900.000000' /></g></svg>"
                  }
                }
              }
            ]
          }
        ]
```

Sur le même modèle que les autres ```annotations```. La propriété ```motivation``` reste *tagging*, et les éléments structurant se situent au niveau de la propriété ```target```, comprenant une propriété ```selector``` > ```value``` pour laquelle on retrouve dans une balise ```<svg>``` les coordonnées des différents points du polygon.

## Manifest intégrant du HTML dans les annotations

L'intégration d'éléments HTML dans les annotations au sein du manifest est possible et documentée sur [cette page](https://iiif.io/api/cookbook/recipe/0019-html-in-annotations/) du consortium IIIF. 
    
### Notes explicatives (traduction de la page du IIIF Consortium)
    
La propriété ```body``` de l'Annotation est un objet de type *TextualBody* qui contient le texte HTML dans une propriété ```value``` et spécifie le format *text/html* dans une propriété ```format``` et éventuellement la langue (par exemple *de* pour l'allemand) dans une propriété ```language```.
Pour intégrer du contenu multilingue on peut créer une liste de plusieurs objets ```TextualBody``` avec différentes propriétés de langue dans la propriété ```body``` de l'Annotation. Mais il est possible que les visualisateurs puissent afficher soit toutes les langues en même temps ou ne pas prendre en charge ce modèle du tout.
    
La Web annotations specification du W3C ne précise pas quels éléments HTML sont autorisés dans le corps d'une annotation, mais toutes les visionneuses limiteront les éléments HTML que l'on peut utiliser dans l'annotation pour des raisons techniques et de sécurité.
    
La IIIF Presentation specification contient des restrictions explicites pour le contenu HTML dans les Property Values (voir [Embedding HTML in descriptive properties](https://iiif.io/api/cookbook/recipe/0007-string-formats/)).
    
Il est conseillé de suivre ces règles comme le plus petit dénominateur commun :
    
1. Le HTML doit être bien formé et doit donc être enveloppé dans un élément tel que ```p``` ou ```span```.
2. Pour signaler à une application que le contenu est du HTML, le premier caractère de votre chaîne doit être ```<``` et le dernier caractère doit être ```>```.
3. Balises que le client peut prendre en charge :  ```a```, ```b```, ```br```, ```i```, ```img```, ```p```, ```small```, ```span```, ```sub``` et ```sup```, ainsi que les attributs ```href``` sur la balise ```a```, et ```src``` et ```alt``` sur la balise ```img```.
    
Si nous avons des exigences qui sortent du cadre de ces règles, nous pouvons configurer une instance personnalisée d'un visualiseur IIIF pour autoriser davantage d'éléments HTML (Mirador définit des règles configurables), mais cela risquerait de limiter l'utilité générale et la réutilisation de nos manifestes et de nos annotations IIIF.

Extrait du [septième manifest](https://gitlab.inha.fr/snr/pervisum/-/blob/main/DATA/experimentations_manifests/manifests_tests/7_manifest_html_in_annotations.json) présent dans ```/manifests_tests```

```json 
"annotations": [
          {
            "id": "https://iiif.io/api/cookbook/recipe/0019-html-in-annotations/canvas-1/annopage-2",
            "type": "AnnotationPage",
            "items": [
              {
                "id": "https://iiif.io/api/cookbook/recipe/0019-html-in-annotations/canvas-1/annopage-2/anno-1",
                "type": "Annotation",
                "motivation": "commenting",
                "body": {
                  "type": "TextualBody",
                  "language": "de",
                  "format": "text/html",
                  "value": "<p>Göttinger Marktplatz mit <a href='https://de.wikipedia.org/wiki/G%C3%A4nseliesel-Brunnen_(G%C3%B6ttingen)'>Gänseliesel Brunnen <img src='https://en.wikipedia.org/static/images/project-logos/enwiki.png' alt='Wikipedia logo'></a></p>"
                },
                "target": "https://iiif.io/api/cookbook/recipe/0019-html-in-annotations/canvas-1"
              }
            ]
          }
        ]
```

## Manifest intégrant des images dans les annotations

    
Extrait du [huitième manifest](https://gitlab.inha.fr/snr/pervisum/-/blob/main/DATA/experimentations_manifests/manifests_tests/8_image_in_annotation.json) présent dans ```/manifests_tests```


```json
"annotations": [
        {
          "id": "https://iiif.io/api/cookbook/recipe/0377-image-in-annotation/canvas-1/annopage-2",
          "type": "AnnotationPage",
          "items": [
            {
              "id": "https://iiif.io/api/cookbook/recipe/0377-image-in-annotation/canvas-1/annopage-2/anno-1",
              "type": "Annotation",
              "motivation": "commenting",
              "body": [
                {
                  "id": "https://iiif.io/api/image/3.0/example/reference/918ecd18c2592080851777620de9bcb5-fountain/full/300,/0/default.jpg",
                  "type": "Image",
                  "format": "image/jpeg"
                },
                {
                  "type": "TextualBody",
                  "language": "en",
                  "value": "Night picture of the Gänseliesel fountain in Göttingen taken during the 2019 IIIF Conference"
                }
              ],
              "target": "https://iiif.io/api/cookbook/recipe/0377-image-in-annotation/canvas-1#xywh=138,550,1477,1710"
            }
          ]
        }
      ]
```


Sur le même modèle que les autres ```annotations```. La propriété ```motivation``` reste *commenting*. Les éléments structurant se situent au niveau de la propriété ```body```, comprenant un objet de type *image* et un objet de type *TextualBody*. La propriété ```target``` contient dans son URL les coordonnées de l'emplacement de l'image (annotation) sur l'image originelle.

