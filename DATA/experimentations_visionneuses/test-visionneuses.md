# Test d'affichage de manifestes dans différentes visionneuses

Comparaison des capacités de visualisation de plusieurs visionneuses dans la documentation IIIF : https://iiif.io/api/cookbook/recipe/matrix/

## Glycerine

Lien vers la démo : https://demo.viewer.glycerine.io/using-embeds

**Fonctionne uniquement avec un manifeste en v3.**

|Exemple de manifeste|Lien vers le visionnage dans la visionneuse|Commentaire|
|--- |--- |--- |
|Une image et une annotation (tag)|https://demo.viewer.glycerine.io/viewer?iiif-content=https://iiif.io/api/cookbook/recipe/0021-tagging/manifest.json||
|Une image et une annotation (image)|https://demo.viewer.glycerine.io/viewer?iiif-content=https://iiif.io/api/cookbook/recipe/0377-image-in-annotation/manifest.json||
|Plusieurs images sans annotations|https://demo.viewer.glycerine.io/viewer?iiif-content=+https://corpus-invisu.inha.fr/iiif/3/10014/manifest||
|Plusieurs images et plusieurs annotations|https://demo.viewer.glycerine.io/viewer?iiif-content=https://margauxfre.github.io/test-manifests/manifests/manifest(1).json|
|Une image avec une annotation HTML|https://demo.viewer.glycerine.io/viewer?iiif-content=https://iiif.io/api/cookbook/recipe/0019-html-in-annotations/manifest.json||
|Une vidéo|https://demo.viewer.glycerine.io/viewer?iiif-content=https://iiif.io/api/cookbook/recipe/0017-transcription-av/manifest.json|Non supporté|
|Plusieurs images (50) et annotations dans un fichier parrallèle|https://demo.viewer.glycerine.io/viewer?iiif-content=https://api.chgov.bar.admin.ch/manifests/32325340/32325340.json|Miniatures des images prennent une bonne partie de la visionneuse + les annotations sont détectées mais ne sont pas affichées|

## Mirador

Pour chaque exemple, il faut ouvrir les annotations dans le menu burger.

Test sur la version de base, sans plugin : https://projectmirador.org/embed/?iiif-content=

|Exemple de manifeste|Lien vers le visionnage dans la visionneuse|Commentaire|
|--- |--- |--- |
|Une image et une annotation (tag)|https://projectmirador.org/embed/?iiif-content=https://iiif.io/api/cookbook/recipe/0021-tagging/manifest.json||
|Une image et une annotation (image)|https://projectmirador.org/embed/?iiif-content=https://iiif.io/api/cookbook/recipe/0377-image-in-annotation/manifest.json|L'image contenue en annotation ne s'affiche pas|
|Plusieurs images sans annotations|https://projectmirador.org/embed/?iiif-content=https://corpus-invisu.inha.fr/iiif/3/10014/manifest||
|Plusieurs images et plusieurs annotations|https://projectmirador.org/embed/?iiif-content=https://margauxfre.github.io/test-manifests/manifests/manifest(1).json||
|Une image avec une annotation HTML|https://projectmirador.org/embed/?iiif-content=https://iiif.io/api/cookbook/recipe/0019-html-in-annotations/manifest.json|
|Une vidéo|https://projectmirador.org/embed/?iiif-content=https://iiif.io/api/cookbook/recipe/0017-transcription-av/manifest.json||
|Plusieurs images (50) et annotations dans un fichier parrallèle|https://projectmirador.org/embed/?iiif-content=https://api.chgov.bar.admin.ch/manifests/32325340/32325340.json|Les annotations  ne sont pas affichées.|

## Annona

Il faut cliquer sur la flèche gauche pour faire apparaître la première annotation.

|Exemple de manifeste|Lien vers le visionnage dans la visionneuse|Commentaire|
|--- |--- |--- |
|Une image et une annotation (tag)|https://ncsu-libraries.github.io/annona/tools/#/display?url=https%3A%2F%2Fiiif.io%2Fapi%2Fcookbook%2Frecipe%2F0021-tagging%2Fmanifest.json&viewtype=iiif-storyboard&manifesturl=&settings=%7B%22fullpage%22%3Atrue%7D||
|Une image et une annotation (image)|https://ncsu-libraries.github.io/annona/tools/#/display?url=https%3A%2F%2Fiiif.io%2Fapi%2Fcookbook%2Frecipe%2F0377-image-in-annotation%2Fmanifest.json&viewtype=&manifesturl=&settings=%7B%7D||
|Plusieurs images sans annotations|https://ncsu-libraries.github.io/annona/tools/#/display?url=https%3A%2F%2Fcorpus-invisu.inha.fr%2Fiiif%2F3%2F10014%2Fmanifest&viewtype=iiif-storyboard&manifesturl=&settings=%7B%22fullpage%22%3Atrue%7D||
|Plusieurs images et plusieurs annotations|https://ncsu-libraries.github.io/annona/tools/#/display?url=https%3A%2F%2Fmargauxfre.github.io%2Ftest-manifests%2Fmanifests%2Fmanifest%281%29.json&viewtype=iiif-storyboard&manifesturl=&settings=%7B%22fullpage%22%3Atrue%7D||
|Une image avec une annotation HTML|https://ncsu-libraries.github.io/annona/tools/#/display?url=https%3A%2F%2Fiiif.io%2Fapi%2Fcookbook%2Frecipe%2F0019-html-in-annotations%2Fmanifest.json&viewtype=iiif-storyboard&manifesturl=&settings=%7B%22fullpage%22%3Atrue%7D|
|Une vidéo|https://ncsu-libraries.github.io/annona/tools/#/display?url=https%3A%2F%2Fiiif.io%2Fapi%2Fcookbook%2Frecipe%2F0017-transcription-av%2Fmanifest.json&viewtype=iiif-storyboard&manifesturl=&settings=%7B%22fullpage%22%3Atrue%7D|Non supporté|
|Plusieurs images (50) et annotations dans un fichier parrallèle|https://ncsu-libraries.github.io/annona/tools/#/display?url=https%3A%2F%2Fapi.chgov.bar.admin.ch%2Fmanifests%2F32325340%2F32325340.json&viewtype=iiif-storyboard&manifesturl=&settings=%7B%22fullpage%22%3Atrue%7D|Erreur : "Error loading image"|

## Adno

Lien : https://adno.app/fr/

Outil d'annotation d'image. On peut donner un manifeste IIIF en entrée mais il ne permet que de sélectionner une image. On travaille sur une image qui est donc sortie de son contexte.

Visionneuse basée sur OpenSeaDragon.

Ne fonctionne pas avec des manifestes en v3.

Des exemples ici : https://adno.app/fr/example/
