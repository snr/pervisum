<?xml version="1.0" encoding="UTF-8"?>
<TEI
change="metopes_publication#openedition"
xmlns="http://www.tei-c.org/ns/1.0"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xmlns:xs="http://www.w3.org/2001/XMLSchema"
xmlns:xlink="http://www.w3.org/1999/xlink"
xmlns:xi="http://www.w3.org/2001/XInclude"
xmlns:ns="http://www.tei-c.org/ns/1.0"
xmlns:mathml="http://www.w3.org/1998/Math/MathML"
xmlns:loext="urn:org:documentfoundation:names:experimental:office:xmlns:loext:1.0"
xmlns:dcr="http://www.isocat.org/ns/dcr"
><teiHeader
><fileDesc
><titleStmt
><title
type="main"
>Voguing ou les normes excédées (Paris, 2015)</title
><author
role="aut"
><name
>Manuel Charpy </name
><affiliation
>CNRS/IRHIS Université de Lille 3</affiliation
></author
><author
role="aut"
><name
>Pablo Grand-Mourcel</name
></author
><author
role="aut"
><name
>Léonor Delaunay </name
></author
></titleStmt
><editionStmt
><edition
><date
>2021-10-19T13:00:00</date
></edition
></editionStmt
><publicationStmt
><ab
type="papier"
><dimensions
><dim
type="pagination"
></dim
></dimensions
><date
></date
></ab
><idno
type="book"
></idno
><ab
type="lodel"
><date
></date
></ab
></publicationStmt
><sourceDesc
><p
>Version Métopes : 2.3</p
><p
>Written by OpenOffice</p
></sourceDesc
></fileDesc
><encodingDesc
><tagsDecl
><rendition
scheme="css"
xml:id="none"
>color:black;</rendition
></tagsDecl
></encodingDesc
><profileDesc
><langUsage
><language
ident="fr-FR"
></language
></langUsage
><textClass
></textClass
></profileDesc
><revisionDesc
><change
when="2021-10-19T13:19:00"
who="Elisa MOUNIER"
>Révision</change
></revisionDesc
></teiHeader
><text
xml:id="text"
><front
><titlePage
><docTitle
><titlePart
style="T_3_Article"
type="main"
>Voguing ou les normes excédées (Paris, 2015)</titlePart
></docTitle
><docAuthor
style="txt_auteur"
>Manuel Charpy</docAuthor
><docAuthor
style="txt_auteur"
>Pablo Grand-Mourcel</docAuthor
><docAuthor
style="txt_auteur"
>Léonor Delaunay</docAuthor
><byline
><affiliation
style="auteur_Institution"
>Manuel Charpy est chargé de recherche au <affiliation
xml:id="aff01"
>CNRS/IRHIS Université de Lille 3</affiliation
>. Il travaille sur l’histoire de la culture matérielle et visuelle aux XIX<hi
rend="sup"
style="typo_Exposant"
>e</hi
> et XX<hi
rend="sup"
style="typo_Exposant"
>e</hi
> siècles en France, au Royaume-Uni et aux États-Unis. Outre de nombreux articles sur l’histoire de la fripe et la SAPE, il prépare un ouvrage tiré de sa thèse (<hi
rend="italic"
style="typo_Italique"
>Le Théâtre des objets. Culture matérielle et identité bourgeoise à Paris au XIX</hi
><hi
rend="sup italic"
style="typo_Exposant_Italic"
>e</hi
><hi
rend="italic"
style="typo_Italique"
> siècle</hi
>, Flammarion, à paraître) ainsi que des ouvrages sur l’histoire des usages sociaux du portrait et sur l’histoire de la fripe.</affiliation
></byline
></titlePage
><argument
><p
style="txt_chapo"
>Enquête par Manuel Charpy avec Léonor Delaunay et Pablo Grand-Mourcel<lb
></lb
>Photographies : Pablo Grand-Mourcel et Manuel Charpy</p
></argument
><div
type="prelim"
><epigraph
><quote
style="txt_Epigraphe"
>«  Le Ball, c’est exprimer une souffrance ressentie et inventer ta vie.  » Lasseindra</quote
></epigraph
></div
></front
><body
><div
type="chapitre"
xml:id="mainDiv"
><figure
><graphic
url="../icono/br/MP-art13-Voguing-fig01.jpg"
></graphic
><p
style="ill-credits-sources"
>Photographie Pablo Grand-Mourcel et Manuel Charpy</p
></figure
><div
type="section1"
><head
style="T_1"
subtype="level1"
>Défilé, beat et lazzis</head
><p
style="txt_Normal"
>Nous retrouvons Yanou et Kylee assis sur les quais de la Seine, face à la gare d’Austerlitz, entre des guinguettes à cocktails chics et des tentes de réfugiés, sirotant une bouteille de bière. Pas d’urgence même s’ils dansent dans une demi-heure : le <hi
rend="italic"
style="typo_Italique"
>ball</hi
> n’a pas de thème ce soir, donc ni maquillage ni vêtements de scène. Ce sera T-Shirt gris flottant frappé d’un étrange « Undesirable n°1 Harry Potter », slim et espadrilles pour Yanou ; casquette large, débardeur ample avec smileys, slim et baskets montantes pour Kylee. Ils s’apprêtent à danser au Wanderlust. La boîte fait craindre un public de voyeurs venu au spectacle de jeunes banlieusards travestis. Yanou et Kylee ne sont pas inquiets, indifférents au lieu : les soirées « Crème de la crème » sont organisées par Lasseindra, leur « mother » de la <hi
rend="italic"
style="typo_Italique"
>House of Ninja</hi
> et elle sait protéger les « sisters ». Quelques badauds passent mais les Vogueurs sont chez eux et font vite oublier la branchitude ennuyée du lieu. Ils savent résister, avec nonchalance, à toute récupération. Le spectacle est annoncé pour 19 h 00 ; il débute vers 22 h 00, sans prévenir.</p
><p
style="txt_Normal"
>Une scène est matérialisée par des cordons de velours qui servent d’ordinaire à contenir ceux qui attendent pour entrer dans la boîte. À une extrémité de ce podium fantasmé, un jury : Sky, danseuse de la House of Ninja, un danseur professionnel habitué de ces battles, aux allures de Tupac, et un homme vêtu d’un ensemble panthère et de lunettes cerclées qui évoquent un intellectuel panafricaniste des années 1960. À l’autre extrémité de la piste, un DJ, et le long des cordons se pressent Vogueurs et proches. Ce soir, c’est avec la House of Ladurée que le <hi
rend="italic"
style="typo_Italique"
>ball</hi
> se joue. Il s’ouvre sur quelques passages brouillons de Vogueurs qui se font copieusement et joyeusement raillées. S’ils déjouent par le travestissement les codes sociaux, les Vogueurs ont élaboré pour la danse et les rituels des <hi
rend="italic"
style="typo_Italique"
>balls</hi
> des codes arbitraires et d’une complexité infinie… ce qui permet toutes les railleries. Le défilé débute. Sur un beat hip-hop et parfois house, une commentatrice – <hi
rend="italic"
style="typo_Italique"
>speaker</hi
> – , seins en obus et juchée sur des talons aiguilles, délivre un flow continu de rap d’une voix de basse rageuse et rocailleuse. Mêlant anglais et français, elle commente les passages, célébrant les réussites et raillant les ratés. Le chœur du public l’accompagne, avec des sortes de mélopées, pour soutenir ou enfoncer les candidates à départager. Car le <hi
rend="italic"
style="typo_Italique"
>Voguing</hi
> c’est aussi, à la manière des battles de rap, du <hi
rend="italic"
style="typo_Italique"
>reading</hi
> – ces moqueries qui fusent – ou du <hi
rend="italic"
style="typo_Italique"
>shade</hi
> – la version avec insultes. Volontiers homophobes, elles doivent protéger de l’homophobie quotidienne.</p
><p
style="txt_Normal"
>Les quolibets sont essentiels car le public doit choisir son camp dans les duels qui se succèdent. L’élimination est directe, dictée par le jury et la foule. Le public fait corps avec sa House ; ce soir-là, il est partagé entre soutien de la House of Ninja et celui de la House of Ladurée – les passages déclenchent un cri singulier chez les uns et « sucré, salé, House La-du-rée » chez les autres. Les deux Houses miment une compétition, faite d’opposition de styles. Pour la House of Ninja on cultive volontiers le style inventé par Willi Ninja qui a créé la maison dans les années 1980 à New York et admirait Fred Astaire, les gestes de gymnastique olympique et les arts martiaux. Mais la House of Ninja Paris a aussi son propre style, impulsé par Lasseindra, fait de chutes spectaculaires et d’humour crâneur. On joue la compétition jusqu’au catch et au burlesque : les candidates se bousculent, s’effondrent, miment la blessure… – dans les années 1980 on mime des combats au couteau – et quand les résultats tombent, elles font mine de faire un scandale, s’effondrent au sol abattues, hurlent à la tricherie… comme un concours de beauté qui tournerait mal.</p
></div
><div
type="section1"
><head
style="T_1"
subtype="level1"
>Paillettes et chaussettes</head
><p
style="txt_Normal"
>Pour le néophyte, les catégories du défilé sont impénétrables. Ironiques, nos Vogueurs tentent de nous les expliquer – en prenant soin d’être inaccessibles. Les unes sont Old way, les autres Up in Drag, Vogue Fem, Butch Queen Face, American runway, European runway… Si elles n’usent pas d’un argot à la manière du Pig latine des Vogueurs de Harlem des années 1980, elles ne cessent de jouer avec le langage. Seule chose claire au final, le terme <hi
rend="italic"
style="typo_Italique"
>Voguing</hi
> vient, on l’aura deviné, du détournement des poses du magazine <hi
rend="italic"
style="typo_Italique"
>Vogue</hi
>. Les défilés parodient tout à la fois les poses des magazines féminins, les défilés de haute-couture et les concours de Miss Beauté – on sort les trophées pour les grandes soirées. Elles ont alors des thèmes, par ailleurs proches de ceux des années 1980 : de « The Face au Cléopatra » « Hollywood »… Pour ces soirées, les Vogueurs bricolent avec grand soin, assemblant vêtements et déguisements chinés ou fabriqués en carton, recouverts de tissus ou peints. Perruque et maquillage volontiers outranciers achèvent la transformation, même si pour beaucoup, elles conservent quelques attributs masculins, barbes soigneusement taillées ou torses poilus.</p
><p
style="txt_Normal"
>Mais le costume n’est pas indispensable : nombre de défilés se font avec les vêtements de tous les jours. C’est le cas ce soir-là au Wanderlust – « venez comme vous êtes » dit l’annonce. Kylee, Naomi Campbell en fourrure et chapka et jambes interminables dans certaines soirées, danse avec son T-Shirt H&amp;M, un short en jean stretch et des chaussettes de sport – un classique chez les Vogueurs. La subversion n’en paraît que plus radicale : le <hi
rend="italic"
style="typo_Italique"
>ball</hi
> et ses allures de music-hall autorise le travestissement et lui donne un aspect carnavalesque. En T-Shirt et chaussettes, la transgression est double : les Vogueurs dynamitent les codes du genre hétérosexuel et dans le même mouvement ceux de la culture gay mainstream, sur laquelle pèse de tout leur poids canons physiques et normes vestimentaires.</p
></div
><div
type="section1"
><head
style="T_1"
subtype="level1"
>Périphéries</head
><p
style="txt_Normal"
>À minuit, la salle se vide avec le couperet du RER. Avec d’autant moins d’hésitations qu’en ce jour de semaine nombre d’entre eux travaillent le lendemain. Quelques-uns sont en apprentissage, d’autres viennent de passer leur bac, d’autres encore cherchent, « du travail pour le bétail », ces jobs précaires et mal payés de vendeurs de fast-food ou de vêtements. Yanu feint de s’étonner que malgré sa « beautiful face », aucun employeur ne le rappelle après plus de vingt CV déposés où une expérience à Auchan drive côtoie une prestation de danse pour Hermès. Pas de caricature : il y a aussi des enfants de la classe moyenne mais l’immense majorité des Vogueurs vient de quartiers dits « populaires » – Charenton, Villiers-sur-Marne, Romainville, Champlan… Pas nécessairement de quartiers « difficiles », mais où il est difficile d’être homosexuel. Ils sont tous issus de l’immigration : « rebeus » et surtout « renois » disent-ils, dont beaucoup des Antilles. Lasseindra vient, elle, de Guyane. Et tous sont gays et le revendiquent, soulignant volontiers que la House of Ninja Paris conserve elle, par rapport à d’autres « chapelles », sa dimension gay et militante.</p
></div
><div
type="section1"
><head
style="T_1"
subtype="level1"
>Houses, mothers and sisters</head
><p
style="txt_Normal"
>Nous avions rendez-vous la semaine précédente pour des prises de vue dans un studio du XX<hi
rend="sup"
style="typo_Exposant"
>e</hi
> arrondissement, rendez-vous fixé quelques semaines avant avec Lasseindra. Tout passe par elle. Je l’avais rencontrée à l’été 2013, devant le kiosque à musique, en face de la Mairie de Charenton. Lasseindra m’avait expliqué, un peu agacée, que toutes mes questions n’avaient pas de sens. Elle se méfiait après de mauvaises expériences avec de grands magazines de mode. Nous avions pris rendez-vous pour le mois suivant ; il eut lieu un an après. Nous avions alors discuté des conditions pour faire des photos des membres de la House of Ninja. Lasseindra exigeait un contrat pour protéger les filles et un droit de regard sur le choix des images.</p
><p
style="txt_Normal"
>En ce mois de juillet 2015, le jour de la prise de vue, elle passe tout vérifier et délègue sa signature à une des « sisters » pour le contrat. Être la « Mother » de la House of Ninja implique d’accompagner ses danseurs comme le ferait la régisseuse d’une troupe. Mais la House est plus que cela : une famille de substitution, comme l’ont été les Houses dans le Harlem des années 1980-90 pour les gays latinos et noirs. En rupture ou en délicatesse avec leur famille, ils trouvent là un refuge. Symbolique sans équivoque, ils abandonnent leur nom de famille pour le remplacer par « Ninja ». Et cette famille, choisie, cultive sa généalogie. Lasseindra appelle les membres ses « babies » qui entre elles s’appellent « sisters ». Et Lasseindra appelle « grand mother » Willi Ninja, le fondateur de la House of Ninja. Nouvelle famille avec ses liens de solidarités qui créent des obligations, notamment de protection.</p
></div
><div
type="section1"
><head
style="T_1"
subtype="level1"
>Poses</head
><p
style="txt_Normal"
>En attendant Lasseindra, installation et tests d’éclairage. En fin de matinée, elles sont toutes là. Après des boutades sur le quartier « qui craint » car il est impossible d’y trouver un KFC, deux partent chercher des menus McDonald. Tess maquille tout le monde. Kylee a une serviette nouée sur la tête pour éviter que ses cheveux longs – qu’elle n’a pas – gênent le maquillage. Dans la cuisine du studio transformée en vestiaire, les Vogueurs déballent leurs sacs de sport et s’habillent.</p
><p
style="txt_Normal"
>Elles savent qu’il s’agit d’un spectacle où seul l’effet et les gestes comptent. On s’habille donc pas cher – GAP souvent, New Look, marques de grandes surfaces, ou de contrefaçons. Ce n’est pas qu’une affaire de moyens : s’ils parlent des marques, notamment de luxe, elles n’ont finalement que peu d’importance dans le <hi
rend="italic"
style="typo_Italique"
>Voguing</hi
>. C’est le bricolage – la création – qui compte. Le vêtement est jeu et déguisement. Le bon goût n’est pas au rendez-vous, les Vogueurs sont ailleurs, ils veulent du goût et du jeu, loin de l’uniforme sobre de la Parisienne. Ils finalisent leurs tenues avec des trombones, élastiques et épingles à nourrice sortis de trousses de lycéens. Ils savent que la mode, celle des magazines, est avant tout une image – ils bricolent comme le font les photographes de mode.</p
></div
><div
type="section1"
><head
style="T_1"
subtype="level1"
>Histoires</head
><p
style="txt_Normal"
>Un Vogueur qui attend d’être photographié en fumant un joint à la fenêtre me recommande les photographies de Chantal Regnault, une des rares enquêtes sur les vogueurs new-yorkais des années 1980, et reprend avec passion l’histoire du <hi
rend="italic"
style="typo_Italique"
>Voguing</hi
>. Si les Vogueurs sapent les clichés du genre, ils bousculent aussi les stéréotypes de la sociologie. À l’évidence, l’idée selon laquelle les « authentiques » subcultures sont sans histoire, car spontanées, ne tient pas. Tous racontent leur histoire du <hi
rend="italic"
style="typo_Italique"
>Voguing</hi
>, parfois mythique, mais en donnent toujours une lecture détaillée. Tous ont vu <hi
rend="italic"
style="typo_Italique"
>Paris is burning</hi
> – du nom d’une soirée – de la documentariste Jennie Livingston, sorti en 1990, et ils le connaissent d’autant mieux qu’il fait une large place à Willi Ninja. Tous tiennent un discours sur la nature du phénomène et sa résurgence. Rien d’artificiel pour autant : s’engager dans le <hi
rend="italic"
style="typo_Italique"
>Voguing</hi
> quand on est un jeune homosexuel issu de l’immigration ne peut se faire à moitié. D’où leur volonté d’échapper à toute folklorisation – elles acceptent soirées et contributions dans quelques clips mais demeurent méfiantes à l’égard de toutes les vampirisations.</p
><p
style="txt_Normal"
>Les Vogueurs – l’enquête reste à mener – font remonter leur histoire au New York des années 1930, moment où se développe la « ball culture ». Elle est alors blanche : sur le modèle des concours de Miss, des concours de danse et des défilés de mode, les participants sont primés pour leurs costumes. Les noirs sont rares et ne peuvent exister qu’en parodiant leur propre identité ou à l’inverse, en se blanchissant. Mais au vrai, les traces de ces pratiques chez les homosexuels noirs sont introuvables – ce qui ne dit rien de leur existence. Quoi qu’il en soit, les Vogueurs remontent volontiers à cet âge mythique qui enracine clairement le <hi
rend="italic"
style="typo_Italique"
>Voguing</hi
> dans la marginalité sociale et la lutte de minorités contre une société oppressante.</p
><p
style="txt_Normal"
>De façon plus assurée, c’est dans les années 1960 à Harlem que quelques homosexuels noirs commencent à organiser des <hi
rend="italic"
style="typo_Italique"
>balls</hi
>, à la croisée des luttes contre la ségrégation et celles du mouvement LGBT naissant. Ces soirées – <hi
rend="italic"
style="typo_Italique"
>Harlem Drag Balls –</hi
> ressemblent aux bals déguisés : stars hollywoodiennes, personnages historiques, de Cléopâtre à Marie-Antoinette, chanteuses en vogue… le tout dans des costumes à paillettes très inspirés du music-hall. Si quelques-uns ont trouvé dans le mouvement Harlem Renaissance, des traces d’une culture gay, elle est globalement mal acceptée d’une lutte contre la ségrégation aux accents culturels et religieux, mais aussi de plus en plus paramilitaire et virile. Les temps changent cependant : à quelques kilomètres d’Harlem, les émeutes de Stonewall dans Greenwich Village en juin 1969 donnent une visibilité nouvelle au mouvement gay et le politise. L’année suivante le lancement d’un défilé appelé « gay pride » à New York et à Los Angeles facilite l’expression publique de son homosexualité et fait tomber la répression policière, très forte y compris à New York : interdiction de se travestir, de servir de l’alcool à des homosexuels, de danser entre hommes…</p
><p
style="txt_Normal"
>Avec les années 1970, le mouvement se développe mais surtout s’organise. La première House semble créée en 1977 par Crystal Labeija. Alors qu’à San Francisco les White Night Riot – à la suite de la faible condamnation de White, assassin d’Harvey Milk – ramènent sur le devant de la scène la question de la condition homosexuelle, des Houses s’ouvrent en dehors de New York, dans les villes à forte culture noire comme Baltimore, Washington et Atlanta. Protectrices, elles offrent un abri à ces jeunes homos en fugue, souvent rejetés par leur famille et qui n’ont pas toujours atteint la majorité sexuelle. Économie précaire, comme le montre <hi
rend="italic"
style="typo_Italique"
>Paris is burning : </hi
>toutes multiplient les petits jobs en journée, et la nuit quelques-unes font du strip-tease dans les boîtes – une infime partie se prostitue. Elles pratiquent le <hi
rend="italic"
style="typo_Italique"
>mopping</hi
>, terme d’argot – peut-être de mop, rafler – pour désigner le shopping qui ne passe pas par la caisse. Mais le soir et souvent toute la nuit durant, à Harlem – au 10 West, 129<hi
rend="sup"
style="typo_Exposant"
>th</hi
> Street – on rebat les cartes. Les Vogueurs, parfois un magazine à la main, singent la surféminité qui s’étale alors dans la presse et la publicité. Mais ils jouent aussi avec les codes de la masculinité et de l’hétérosexualité dans des catégories dédiées aux militaires, aux étudiants modèles des grandes écoles, aux cadres dynamiques de Wall Street… Dans les nuits de Harlem, cette jeunesse désargentée mime la norme de la réussite, incarnée par Manhattan. La norme, comme excédée, exacerbée, s’effondre joyeusement sous le poids de ses codes.</p
><p
style="txt_Normal"
>Avec les années 1980, le mouvement connaît une double mutation. Avec l’arrivée du SIDA, les Houses prennent d’autant plus d’importance que la solidarité et le soutien sont essentiels face à la maladie et au rejet des familles. D’autre part, la culture des <hi
rend="italic"
style="typo_Italique"
>balls</hi
> est de plus en plus visible : en 1990, alors que sort le documentaire <hi
rend="italic"
style="typo_Italique"
>Paris is burning</hi
> qui rencontre un relatif succès, le clip <hi
rend="italic"
style="typo_Italique"
>Vogue</hi
> de l’ogre Madonna donne à voir le <hi
rend="italic"
style="typo_Italique"
>Voguing</hi
> au grand public. Les Houses se multiplient. Il y a celles qui portent le nom de leur fondateur – House of Pendavis, House of Duprée, House of Corey, House of Ninja –, celle qui revendiquent leur identité – House of Ebony, House of Overness… –, puis celles qui portent volontiers le nom d’une marque – House of Saint Laurent, House of Dior, House of Chanel, House of Miyake-Mugler, House of Balenciaga, House of Ladurée… Authenticité perdue par la société du spectacle ? Dès 1989, Willi Ninja regrette les anciens <hi
rend="italic"
style="typo_Italique"
>balls</hi
>, la disparition de la vie musicale et sociale des rues de New York… Si <hi
rend="italic"
style="typo_Italique"
>Paris is burning</hi
> ressemble à un chant du cygne, c’est qu’il exprime la nostalgie d’un monde, celui d’avant le sida.</p
></div
><div
type="section1"
><head
style="T_1"
subtype="level1"
>Généalogies</head
><p
style="txt_Normal"
>Mais le <hi
rend="italic"
style="typo_Italique"
>Voguing</hi
> est aujourd’hui vivant. Sa résurgence n’a rien d’inattendu. Son développement n’est peut-être pas sans rapport avec la culture gay du Paris des années 1920-1930. Au demeurant, comme le hip-hop, c’est une culture qui travaille de façon souterraine. Fruit des exclusions multiples et d’une série de normes à déjouer, elle a toutes les raisons de perdurer. Contrairement à ce qu’a pu affirmer la sociologie, les cultures populaires ne vivent pas qu’une fois, et leurs redites ne sont pas nécessairement commerciales, dévoyées… La conscience et la connaissance de son histoire ne sont pas synonymes de la perte d’une hypothétique authenticité. Les Vogueurs d’aujourd’hui rejouent peut-être le <hi
rend="italic"
style="typo_Italique"
>Voguing</hi
> des années 1980, celui des noirs et des Latinos gays de Harlem. Mais rien de folklorique ici : les Vogueurs sont noirs et arabes, gays, dans un monde qui les exclue du travail du fait de leurs origines, dans un environnement proche qui les exclue car gays, enfin, dans un monde gay qui les excluent car pauvres. Pour eux, le fil de cette histoire ne s’est jamais brisé. Lasseindra n’appelle-t-il pas Willi Ninja, mort à 45 ans en 2006, « grand mother » et Archie Burnett, danseur noir qui vogue en homme, « grand père » ?</p
><p
style="txt_Normal"
>Les sisters jettent un œil amusé aux photographies prises et en choisissent quelques-unes pour leurs books. Démaquillage rapide : elles sont attendues dans une soirée. Certains partent hilares avec des traces de maquillage, goûtant par avance les réactions des passants dans la rue.</p
></div
><div
type="section1"
><head
style="T_1"
subtype="level1"
><ref
target="https://devisu.inha.fr/modespratiques/123?file=1"
>Portfolio : 8 juillet 2015, Paris XX<hi
rend="sup"
style="typo_Exposant"
>e</hi
> arrondissement</ref
></head
><p
style="txt_Normal"
><ref
target="https://devisu.inha.fr/modespratiques/123?file=1"
>https://devisu.inha.fr/modespratiques/123?file=1</ref
></p
><figure
><graphic
url="../icono/br/MP-art13-Voguing-fig17.jpg"
></graphic
></figure
></div
><div
type="section1"
><head
style="T_1"
subtype="level1"
>Bibliographie</head
><bibl
style="txt_Bibliographie"
>Molly <hi
rend="small-caps"
style="typo_SC"
>McGarry</hi
> et Fred <hi
rend="small-caps"
style="typo_SC"
>Wasserman</hi
>, <hi
rend="italic"
style="typo_Italique"
>Becoming Visible: An Illustrated History of Lesbian and Gay Life in Twentieth-century America</hi
>, New York, Penguin Books, 1998.</bibl
><bibl
style="txt_Bibliographie"
>A. B. Christa <hi
rend="small-caps"
style="typo_SC"
>Schwarz</hi
>, <hi
rend="italic"
style="typo_Italique"
>Gay voices of Harlem Renaissance</hi
>, Indiana University Press, 2003.</bibl
><bibl
style="txt_Bibliographie"
>Marlis <hi
rend="small-caps"
style="typo_SC"
>Schweitzer</hi
>, <hi
rend="italic"
style="typo_Italique"
>When Broadway was the Runway: Theater, Fashion and American Culture</hi
>, University of Pennsylvania Press, 2009.</bibl
><bibl
style="txt_Bibliographie"
><hi
rend="italic"
style="typo_Italique"
>Voguing: Voguing and the House Ballroom Scene of New York City</hi
>, <hi
rend="italic"
style="typo_Italique"
>1989-92</hi
>, photographs by Chantal Regnault, Introduction by Tim Lawrence, Soul Jazz Books, 2011.</bibl
><bibl
style="txt_Bibliographie"
>Tiphaine <hi
rend="small-caps"
style="typo_SC"
>Bressin</hi
> et Jérémy <hi
rend="small-caps"
style="typo_SC"
>Patinier</hi
>, <hi
rend="italic"
style="typo_Italique"
>Strike a pose : Histoires(s) du voguing</hi
>, Paris, Éditions Des ailes sur un tracteur, 2012.</bibl
></div
><div
type="section1"
><head
style="T_1"
subtype="level1"
>Documentaire</head
><bibl
style="txt_Bibliographie"
>Jennie <hi
rend="small-caps"
style="typo_SC"
>Livingstone</hi
>, <hi
rend="italic"
style="typo_Italique"
>Paris is burning</hi
>, 1990, 78 mn.</bibl
><bibl
style="txt_Bibliographie"
>Frédéric <hi
rend="small-caps"
style="typo_SC"
>Nauczyciel</hi
><hi
rend="italic"
style="typo_Italique"
>, Vogue</hi
><hi
rend="italic"
style="typo_Italique"
> </hi
><hi
rend="italic"
style="typo_Italique"
>! Baltimore</hi
>, 2011, photographies et installation.</bibl
></div
></div
></body
></text
></TEI
>
